package br.ucsal;

public class Carro {
	private String modelo = "Fusca";
	private Motor motor = new Motor("1.0");

	public Carro(Motor motor, String modelo) {
		this.motor = motor;
		this.modelo = modelo;
	}
	
	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public Motor getMotor() {
		return motor;
	}

	public void setMotor(Motor motor) {
		this.motor = motor;
	}

	public String toString() {
		return this.modelo +' '+  motor.getPotencia();
	}
}
