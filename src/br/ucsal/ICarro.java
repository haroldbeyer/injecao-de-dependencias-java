package br.ucsal;

public interface ICarro {
	String getModelo();
	
	void SetModelo(String modelo);
	
	Motor getMotor();
	
	void setMotor(Motor motor);
	
	String toString();
	
}
