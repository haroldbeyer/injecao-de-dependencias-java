package br.ucsal;

import java.lang.reflect.InvocationTargetException;

public class Mecanico {
	private Carro carro;
	
	public Mecanico(Carro carro) {
		this.carro = carro;
	}
	
	public Carro getCarro() {
		return carro;
	}

	public void setCarro(Carro carro) {
		this.carro = carro;
	}

	public static Mecanico getInstancia(String carro, String potencia) throws ReflectiveOperationException, Exception, Throwable, InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException {
		Carro carro1 = (Carro) Class.forName(carro).getDeclaredConstructor().newInstance();
		carro1.setMotor(new Motor(potencia));
		return new Mecanico(carro1);
	}
}
