package br.ucsal;

public class Motor {
	private String potencia = "1.0";
	
	public Motor(String potencia) {
		super();
		this.potencia = potencia;
	}

	public String getPotencia() {
		return potencia;
	}

	public void setPotencia(String potencia) {
		this.potencia = potencia;
	}
}